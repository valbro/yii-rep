var manufacturer = 0;
var category = 0;


$(function(){
	
	$(".menu-element .add-element").on("click",  function() {
		$(".add-good-element.popup-element").fadeIn();
		$("body").addClass("locked");
	});
	
	$(".manufacturers-element .add-element").on("click",  function() {
		$(".add-manufacturer-element.popup-element").fadeIn();
		$("body").addClass("locked");
	});
	
	
	$(".categories-element .add-element").on("click",  function() {
		$(".add-category-element.popup-element").fadeIn();
		$("body").addClass("locked");
	});
	
	$(".groups-element .add-element").on("click",  function() {
		$(".add-group-element.popup-element").fadeIn();
		$("body").addClass("locked");
	});
	
	$(".menu-element .good-element img").on("click",  function() {
		$(this).parent().parent().find(".popup-element").fadeIn();
		$("body").addClass("locked");
	});
	
	$(".groups-element").on("click", ".save-element", function() {
		var $this = $(this);
		var id = $this.data("id");
		var ids = [];
		$(".good-element [type=checkbox]:checked").each(function(i, elem) {
			var good_id = $(this).data("id");
			ids.push(good_id);
		});
		
		$.ajax({
			"url" : "/group/saveGoods",
			"type" : "post",
			"data" : {"group_id":id,
					  "ids":JSON.stringify(ids)
					  },
			"success" : function(data) {
				window.location.href = $this.parent().find("a").attr("href");
			}
		});
	});
	
	$("body").on("click", ".popup-element .close", function(e) {
		$(e.currentTarget).parent().fadeOut();
		$("body").removeClass("locked");
	});

	$(".add-good-element.popup-element").on("click", ".add", function(e) {
		var $this = $(e.delegateTarget);
		
		$.ajax({
			"url" : "/good/add",
			"type" : "post",
			"data" : {"name":$this.find('.name').val(),
					  "price":$this.find('.price').val(),
					  "params":$this.find('.params').val(),
					  "article":$this.find('.article').val(),
					  "category_id":$this.find('.category').val(),
					  "manufacturer_id":$this.find('.manufacturer').val(),
					  "group_id":$this.find('.group').val()
					  },
			"success" : function(data) {
				$this.parent().parent().fadeOut();
				$("body").removeClass("locked");
				window.location.href = 
					"?category_id=" + $this.find('.category').val() + 
					"&group_id=" + $this.find('.group').val() + 
					"&manufacturer_id=" + $this.find('.manufacturer').val();
			}
		})
	});
	
	$("body").on("click", ".redact-good-element.popup-element .save", function(e) {
		var $this = $(e.delegateTarget);
		var id = $(this).data("id");
		$.ajax({
			"url" : "/good/redact",
			"type" : "post",
			"data" : {"id" : id,
					  "name":$this.find('.name').val(),
					  "price":$this.find('.price').val(),
					  "params":$this.find('.params').val(),
					  "article":$this.find('.article').val(),
					  "category_id":$this.find('.category').val(),
					  "manufacturer_id":$this.find('.manufacturer').val()
					  },
			"success" : function(data) {
				window.location.reload();
			}
		})
	});

	$(".good-element").on("click", ".remove", function(e) {
		e.preventDefault();
		var $this = $(this);
		var id = $this.parent().data("id");
		if (confirm("удалить выбранный товар?")) {
			$.ajax({
				"url" : "/good/del",
				"type" : "post",
				"data" : {"id":id},
				"success" : function(data) {
					$this.parent().parent().fadeOut();
					$("body").removeClass("locked");
					window.location.reload();
				}
			})
		}
		return false;
	});

	$(".add-category-element.popup-element").on("click", ".add", function(e) {
		var $this = $(e.delegateTarget);
		var id = $this.parent().data("id");
		$.ajax({
			"url" : "/category/add",
			"type" : "post",
			"data" : {"id":id},
			"data" : {"name":$this.find('.name').val()},
			"success" : function(data) {
				$this.parent().parent().fadeOut();
				$("body").removeClass("locked");
				window.location.reload();
			}
		})
		
	});
	$(".categories-element").on("click", ".remove", function() {
		var $this = $(this);
		var id = $this.parent().data("id");
		if (confirm("удалить выбранную категорию?")) {
			$.ajax({
				"url" : "/category/del",
				"type" : "post",
				"data" : {"id":id},
				"success" : function(data) {
					$this.parent().parent().fadeOut();
					$("body").removeClass("locked");
					window.location.reload();
				}
			});
		}
	});
	
	$(".add-group-element.popup-element").on("click", ".add", function(e) {
		var $this = $(e.delegateTarget);
		var id = $this.parent().data("id");
		$.ajax({
			"url" : "/group/add",
			"type" : "post",
			"data" : {"id":id},
			"data" : {"name":$this.find('.name').val()},
			"success" : function(data) {
				$this.parent().parent().fadeOut();
				$("body").removeClass("locked");
				window.location.reload();
			}
		});
	});
	$(".groups-element").on("click", ".remove", function() {
		var $this = $(this);
		var id = $this.parent().data("id");
		if (confirm("удалить группу?")) {
			$.ajax({
				"url" : "/group/del",
				"type" : "post",
				"data" : {"id":id},
				"success" : function(data) {
					$this.parent().parent().fadeOut();
					$("body").removeClass("locked");
					window.location.reload();
				}
			});
		}
	});
	

	$(".add-manufacturer-element.popup-element").on("click", ".add", function(e) {
		var $this = $(e.delegateTarget);
		var id = $this.data("id");
		$.ajax({
			"url" : "/manufacturer/add",
			"type" : "post",
			"data" : {"id":id},
			"data" : { "name":$this.find('.name').val()},
			"success" : function(data) {
				$this.parent().parent().fadeOut();
				$("body").removeClass("locked");
				window.location.reload();
			}
		});
	});
	
	$(".open-redact-element").on("click", function() {
		var id = $(this).data("id");
		$.ajax({
				"url" : "/good/redactForm?good_id=" + id,
				"type" : "get",
				"success" : function(html) {
					$(".redact-place-element").html(html);
					$(".redact-good-element").fadeIn();
					
					//window.location.reload();
				}
			});
	});
	
	$(".manufacturers-element").on("click", ".remove", function() {
		var $this = $(this);
		var id = $this.parent().data("id");
		if (confirm("удалить выбранного производителя?")) {
			$.ajax({
				"url" : "/manufacturer/del",
				"type" : "post",
				"data" : {"id":id},
				"success" : function(data) {
					$this.parent().parent().fadeOut();
					$("body").removeClass("locked");
					window.location.reload();
				}
			});
		}
		
	});
});
