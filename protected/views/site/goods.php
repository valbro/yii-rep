<?php
/* @var $this SiteController */
/* @var $model AddgoodForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Goods';
$this->breadcrumbs=array(
	'Goods',
);
?>

<h1>Goods</h1>

<p>Please add a good:</p>

<div class="add-good-element popup-element">
	<span class="close"><i class="fa fa-close"></i></span>
	<h2>Добавить товар</h2>
	<div class="form">

	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'addgood-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>

		<p class="note">Fields with <span class="required">*</span> are required.</p>

		<?php echo $form->errorSummary($model); ?>

		<div class="row">
			<?php echo $form->labelEx($model,'title'); ?>
			<?php echo $form->textField($model,'title'); ?>
			<?php echo $form->error($model,'title'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model,'content'); ?>
			<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'content'); ?>
		</div>

		<div class="row buttons">
			<?php echo CHtml::submitButton('Добавить'); ?>
		</div>

	<?php $this->endWidget(); ?>

	</div><!-- form -->
</div>

<div class="menu-element">
	<ul>

		<li>
			<div class="add-element">
				<i class="fa fa-plus" style="font-size: 70px; line-height: 140px;"></i>
			</div>
		</li>
			<?php $tmp=AddgoodForm::model()->findAll(); ?>
			<?php foreach ($tmp as $goods) { ?>
			<li>
			<div class="good-element" data-id="<?php echo $goods["id"]; ?>">
				<span class="remove"><i class="fa fa-close"></i></span>
				<div><input type="checkbox" data-id="<?php echo $goods["id"]; ?>"></input>название:
					<?php echo $goods["title"]; ?>
				</div>
				<img width="100" src="<?php echo Yii::app()->request->baseUrl; ?>/images/dummy.png"></img>
				<div>цена: <?php echo $goods["content"]; ?></div>
			</div>
			<div class="popup-element full-card">
				<span class="close"><i class="fa fa-close"></i></span>
				<h2>Полное описание</h2>
				<table>
					<tr><td>название: </td><td>
						<?php echo $goods["title"]; ?>
					</td></tr>
					<tr><td>цена: </td><td><?php echo $goods["content"]; ?></td></tr>
				</table>
				<button class="open-redact-element" data-id="<?php echo $goods["id"]; ?>">редактировать</button>
			</div>
			</li>
			<?php } ?>
	</ul>
</div>
