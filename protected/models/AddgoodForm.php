<?php
class AddgoodForm extends CActiveRecord implements IECartPosition
{
	#public $title;
	#public $content;
	
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getId()
	{
        return 'Good'.$this->id;
    }

    public function getPrice()
	{
        return $this->content;
    }

	#public function primaryKey()
	#{
    #	return 'id';
	#}

    public function tableName()
    {
        return 'addgoodform';
    }
    public function rules()
    {
        return array(
            array('title, content', 'required'),
        );
	}
}
?>
