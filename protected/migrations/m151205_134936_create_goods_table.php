<?php

class m151130_203259_create_news_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('addgoodform', array(
            'id' => 'pk',
            'title' => 'string NOT NULL',
            'content' => 'text',
        ));
	}

	public function down()
	{
		$this->dropTable('addgoodform');
		echo "m151130_203259_create_news_table does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
